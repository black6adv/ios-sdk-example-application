import UIKit
import Black6AdvSDK

class ViewController: UIViewController {
    @IBOutlet weak var banner: B6BannerView!
    
    var rotator: B6BannerRotator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        rotator = B6BannerRotator.init(bannerView: banner)
        rotator.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

