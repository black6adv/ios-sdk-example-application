#ifndef B6BannerType_h
#define B6BannerType_h

/** The type of the banner displayed inside the View */
typedef NS_ENUM(NSInteger, B6BannerType) {
    /** Small banner of 300×50 pixels. This is the default */
    SMALL = 0,
    /** Big banner of 300×100 pixels */
    LARGE = 2,
    /** A wider version of SMALL. 320×50 pixels */
    WIDE = 4,
};

#endif
