#import <Foundation/Foundation.h>

/** Object used to track the installations of Applications on iOS */
@interface B6Tracker : NSObject

/** Function called to check if the application was just installed */
+ (void)installationTracking;


@end
