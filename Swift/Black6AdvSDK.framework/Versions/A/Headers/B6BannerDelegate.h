#import <Foundation/Foundation.h>
#import "B6BannerView.h"

/** Delegate used by the banner view to notify of certain event */
@protocol B6BannerDelegate <NSObject>

/** Called when the request of a new Banner from the server is completed */
@optional
- (void)bannerReady;

/** Called after the view is made visible */
@optional
- (void)bannerVisible;

/** Called after the banner is removed from the view */
@optional
- (void)bannerHidden;

@end
