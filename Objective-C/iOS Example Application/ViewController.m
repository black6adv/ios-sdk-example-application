#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

B6BannerRotator *rotator;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    rotator = [B6BannerRotator rotatorWithBannerView:self.banner];
    [rotator start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
