#import <Foundation/Foundation.h>
#import "B6BannerView.h"

/** A simple banner rotator for B6BannerView with timings that 
 can be configured */
@interface B6BannerRotator : NSObject<B6BannerDelegate>

/** The banner view controlled by this rotator */
@property(strong,nonatomic) B6BannerView* bannerView;

/** Show the banner after this amount of seconds */
@property(nonatomic) int showAfter;

/** Keep the banner on the screen for this ammount of seconds then hides it */
@property(nonatomic) int hideAfter;

/** Make a request for a new banner after this ammount of seconds */
@property(nonatomic) int showAgainAfter;

/** Create a new instance of the rotator using a banenr view */
+ (instancetype)rotatorWithBannerView:(B6BannerView*)bannerView;

/** Start the banner rotator */
- (void)start;

@end
